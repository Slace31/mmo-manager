import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'fse-games-page',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './games-page.component.html',
  styleUrls: ['./games-page.component.scss'],
})
export class GamesPageComponent {}
