import { Route } from '@angular/router';
import { GamesPageComponent } from './pages/games-page/games-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

export const appRoutes: Route[] = [
  { path: 'games', component: GamesPageComponent },
  { path: '', component: HomePageComponent },
];
